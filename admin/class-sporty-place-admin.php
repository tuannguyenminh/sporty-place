<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.linkedin.com/in/minhtuan2086/
 * @since      1.0.0
 *
 * @package    Sporty_Place
 * @subpackage Sporty_Place/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Sporty_Place
 * @subpackage Sporty_Place/admin
 * @author     Minh Tuan Nguyen <minhtuan2086@gmail.com>
 */
class Sporty_Place_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sporty_Place_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sporty_Place_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sporty-place-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sporty_Place_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sporty_Place_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sporty-place-admin.js', array( 'jquery' ), $this->version, false );

	}


	// Register Custom Post Type
	public function place_post_type() {

			$labels = array(
				'name'                  => _x( 'Sport Places', 'Post Type General Name', 'sporty' ),
				'singular_name'         => _x( 'Sport Place', 'Post Type Singular Name', 'sporty' ),
				'menu_name'             => __( 'Sporty Place', 'sporty' ),
				'name_admin_bar'        => __( 'Sporty Place', 'sporty' ),
				'archives'              => __( 'Place Archives', 'sporty' ),
				'attributes'            => __( 'Item Attributes', 'sporty' ),
				'parent_item_colon'     => __( 'Parent Item:', 'sporty' ),
				'all_items'             => __( 'All Items', 'sporty' ),
				'add_new_item'          => __( 'Add New Item', 'sporty' ),
				'add_new'               => __( 'Add New Place', 'sporty' ),
				'new_item'              => __( 'New Item', 'sporty' ),
				'edit_item'             => __( 'Edit Item', 'sporty' ),
				'update_item'           => __( 'Update Item', 'sporty' ),
				'view_item'             => __( 'View Item', 'sporty' ),
				'view_items'            => __( 'View Items', 'sporty' ),
				'search_items'          => __( 'Search Item', 'sporty' ),
				'not_found'             => __( 'Not found', 'sporty' ),
				'not_found_in_trash'    => __( 'Not found in Trash', 'sporty' ),
				'featured_image'        => __( 'Featured Image', 'sporty' ),
				'set_featured_image'    => __( 'Set featured image', 'sporty' ),
				'remove_featured_image' => __( 'Remove featured image', 'sporty' ),
				'use_featured_image'    => __( 'Use as featured image', 'sporty' ),
				'insert_into_item'      => __( 'Insert into item', 'sporty' ),
				'uploaded_to_this_item' => __( 'Uploaded to this item', 'sporty' ),
				'items_list'            => __( 'Items list', 'sporty' ),
				'items_list_navigation' => __( 'Items list navigation', 'sporty' ),
				'filter_items_list'     => __( 'Filter items list', 'sporty' ),
			);

			$args = array(
				'label'                 => __( 'Sport Place', 'sporty' ),
				'description'           => __( 'Store information for Place', 'sporty' ),
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
				'hierarchical'          => false,
				'public'                => true,
				'show_ui'               => true,
				'show_in_menu'          => true,
				'menu_position'         => 5,
				'menu_icon'             => 'dashicons-sticky',
				'show_in_admin_bar'     => true,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => true,
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'capability_type'       => 'page',
				'show_in_rest'          => true,
				'rest_base'             => 'places',
				'taxonomies'		  			=> array('topics'),
			);
			register_post_type( 'sporty_place', $args );

		}

		// Register Custom Taxonomy
	  public function custom_sporty_taxonomy() {

	 	$labels = array(
	 		'name'                       => _x( 'Fields', 'Taxonomy General Name', '' ),
	 		'singular_name'              => _x( 'Field', 'Taxonomy Singular Name', '' ),
	 		'menu_name'                  => __( 'Sporty Category', '' ),
	 		'all_items'                  => __( 'All Items', '' ),
	 		'parent_item'                => __( 'Parent Item', '' ),
	 		'parent_item_colon'          => __( 'Parent Item:', '' ),
	 		'new_item_name'              => __( 'New Item Name', '' ),
	 		'add_new_item'               => __( 'Add New Item', '' ),
	 		'edit_item'                  => __( 'Edit Item', '' ),
	 		'update_item'                => __( 'Update Item', '' ),
	 		'view_item'                  => __( 'View Item', '' ),
	 		'separate_items_with_commas' => __( 'Separate items with commas', '' ),
	 		'add_or_remove_items'        => __( 'Add or remove items', '' ),
	 		'choose_from_most_used'      => __( 'Choose from the most used', '' ),
	 		'popular_items'              => __( 'Popular Items', '' ),
	 		'search_items'               => __( 'Search Items', '' ),
	 		'not_found'                  => __( 'Not Found', '' ),
	 		'no_terms'                   => __( 'No items', '' ),
	 		'items_list'                 => __( 'Items list', '' ),
	 		'items_list_navigation'      => __( 'Items list navigation', '' ),
	 	);
	 	$args = array(
	 		'labels'                     => $labels,
	 		'hierarchical'               => false,
	 		'public'                     => true,
	 		'show_ui'                    => true,
	 		'show_admin_column'          => true,
	 		'show_in_nav_menus'          => true,
	 		'show_tagcloud'              => true,
	 	);
	 	register_taxonomy( 'sporty_field', array( 'sporty_place' ), $args );

	 }





}
