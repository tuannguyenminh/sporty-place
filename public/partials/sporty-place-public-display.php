<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.linkedin.com/in/minhtuan2086/
 * @since      1.0.0
 *
 * @package    Sporty_Place
 * @subpackage Sporty_Place/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
