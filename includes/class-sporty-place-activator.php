<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.linkedin.com/in/minhtuan2086/
 * @since      1.0.0
 *
 * @package    Sporty_Place
 * @subpackage Sporty_Place/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sporty_Place
 * @subpackage Sporty_Place/includes
 * @author     Minh Tuan Nguyen <minhtuan2086@gmail.com>
 */
class Sporty_Place_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		
	}

}
